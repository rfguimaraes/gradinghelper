<!--
SPDX-FileCopyrightText: 2022 Ricardo Guimarães

SPDX-License-Identifier: CC0-1.0
-->

# Grading Helper

Just a simple tool to help me grade assignments which data coming from CSV files.

It required three arguments as input: 
1. a path to a CSV containing each question item, and the respective value
2. a path to a CSV containing the discounts per student. It should have the following format:

|Question Item | Discount Type | Discount Value | ID1 | ID2 | ... |
|--------------|---------------|----------------|-----|-----|-----|
|text        | text     | number | 0 or 1 | 0 or 1 | ... |
|...|...|...|...|...|

(The last row is used to apply the late delivery penalty multiplier)

3. Path to an output directory for the feedback text files
