# SPDX-FileCopyrightText: 2022 Ricardo Guimarães
#
# SPDX-License-Identifier: Apache-2.0

"""Tests for QuestionItem"""
import unittest

from ..src.QuestionItem import QuestionItem


class QuestionItemTest(unittest.TestCase):
    """Test for QuestionItem"""
    def test_create(self):
        name = "test question item"
        q_item = QuestionItem(name)
        self.assertEqual(q_item.name, name)
