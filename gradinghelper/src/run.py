import sys
import pandas as pd
from parsing import AssignmentBuilder, SubmissionBuilder
from reporter import Reporter


if __name__ == "__main__":
    quest_df = pd.read_csv(sys.argv[1])
    sub_df = pd.read_csv(sys.argv[2])

    assignment_builder = AssignmentBuilder()
    assignment = assignment_builder.build_assignment(quest_df)

    submissions_builder = SubmissionBuilder()
    submissions = submissions_builder.build_submissions(sub_df)

    reporter = Reporter(assignment)

    for submission in submissions:
        reporter.output_report(sys.argv[3], submission)
