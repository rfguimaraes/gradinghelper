from data import Discount, Comment, QuestionItem
from data import Assignment, Answer, Submission
from enum import Enum
from slugify import slugify


class RowType(Enum):
    DISCOUNT = 1
    COMMENT = 2
    LATE_FACTOR = 3


class AssignmentBuilder:
    def build_assignment(self, df):
        assignment = Assignment(self._build_question_items(df))
        self._build_question_items(df)
        return assignment

    def _build_question_items(self, df):
        question_items = dict()
        for row in df.itertuples():
            qi = QuestionItem(row[1], float(row[2]))
            question_items[qi.name] = qi
        return question_items


class SubmissionBuilder:
    def __init__(self):
        self.comments = dict()
        self.discounts = dict()

    def _col_to_id(self, col):
        try:
            return int(col)
        except ValueError:
            return slugify(col)

    def build_submissions(self, df):
        submissions = list()
        for col in df.columns[3:]:
            submission = self._build_submission(df, col)
            submissions.append(submission)
        return submissions

    def _build_submission(self, df, col):
        relevant_cols = ["Question Item", "Discount Type", "Discount Value"]
        relevant_cols.append(str(col))
        sub_df = df[relevant_cols]
        sub_df = sub_df[sub_df[str(col)] != 0]
        answers = dict()
        comments = list()
        late_factor = 1.0
        for qi in sub_df['Question Item'].unique().tolist():
            qi_df = sub_df[sub_df["Question Item"] == qi]
            for row in qi_df.itertuples():
                indicator, content = self._parse_row(row)
                if indicator is RowType.DISCOUNT:
                    if qi not in answers:
                        answers[qi] = Answer(qi)
                    answers[qi].discounts.append(content)
                elif indicator is RowType.COMMENT:
                    comments.append(content)
                elif indicator is RowType.LATE_FACTOR:
                    late_factor = content
        return Submission(self._col_to_id(col), answers, comments, late_factor)

    def _parse_row(self, row):
        if row.Index in self.discounts.keys():
            return (RowType.DISCOUNT, self.discounts[row.Index])
        elif row.Index in self.comments.keys():
            return (RowType.COMMENT, self.comments[row.Index])
        elif row[1] == 'Late':
            return (RowType.LATE_FACTOR, float(row[4]))
        elif row[1].startswith("Warning") or row[1].startswith("Comment"):
            self.comments[row.Index] = Comment(row.Index, row[1], row[2])
            return (RowType.COMMENT, self.comments[row.Index])
        else:
            discount = Discount(row.Index, row[1], row[2], row[3])
            self.discounts[row.Index] = discount
            return (RowType.DISCOUNT, self.discounts[row.Index])
