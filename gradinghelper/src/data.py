# SPDX-FileCopyrightText: 2022 Ricardo Guimarães
#
# SPDX-License-Identifier: Apache-2.0

"""Basic dataclasses"""
from dataclasses import dataclass, field


@dataclass
class QuestionItem:
    """An item that is worth points in an assignment"""
    name: str
    max_value: float = 1.0


@dataclass
class Assignment:
    """A set of question items"""
    items: dict[QuestionItem]


@dataclass
class Discount:
    """Discounts on question items"""
    disc_id: int
    qitem_id: str
    text: str
    value: float

    def to_message(self):
        return f"{self.qitem_id:} {self.text} ({self.value})"


@dataclass
class Comment:
    """Simple comment"""
    com_id: int
    header: str
    text: str

    def to_message(self):
        return f"{self.header}: {self.text}"


@dataclass
class Answer:
    """An answer to a question item"""
    qitem_id: str
    discounts: list[Discount] = field(default_factory=list)


@dataclass
class Submission:
    sub_id: [int, str]
    answers: dict[Answer]
    comments: list[Comment]
    late_factor: float = 1.0
