from pathlib import Path


class Reporter:
    def __init__(self, assignment):
        self.assignment = assignment
        self.assignment_max = None
        self._calculate_max_score()

    def _calculate_max_score(self):
        self.assignment_max = 0
        for qi_val in self.assignment.items.values():
            self.assignment_max += qi_val.max_value

    def _process_discounts(self, submission):
        score = 0
        messages = []
        for qi_key in sorted(self.assignment.items):
            qi_val = self.assignment.items[qi_key]
            total_disc = 0
            discount_messages = []
            if qi_key in submission.answers:
                for discount in submission.answers[qi_key].discounts:
                    discount_messages.append("- " + discount.to_message())
                    total_disc += discount.value
            qi_score = max(qi_val.max_value + total_disc, 0)
            messages.append(f"{qi_key}: {qi_score}/{qi_val.max_value}")
            messages.extend(discount_messages)
            score += qi_score
        return score, messages

    def _generate_feedback(self, submission):
        all_messages = []
        (score, disc_messages) = self._process_discounts(submission)
        comment_messages = [c.to_message() for c in submission.comments]
        all_messages.append("Feedback:")
        all_messages.extend(disc_messages)
        if submission.late_factor != 1.0:
            prev_score = score
            score = score*(1 - submission.late_factor)
            late_penalty = submission.late_factor
            change_score_text = f"({prev_score} -> {score})"
            late_msg = "Late delivery: {0:.0%} penalty ".format(late_penalty)
            all_messages.append(" ".join([late_msg, change_score_text]))
        all_messages.append(f"\nTotal: {score}/{self.assignment_max}\n")
        for comment in sorted(comment_messages):
            all_messages.append(comment)
        return all_messages

    def output_report(self, path, submission):
        with Path(path, f"{submission.sub_id}.txt").open("w") as outfile:
            messages = self._generate_feedback(submission)
            for msg in messages:
                outfile.write(msg + "\n")
